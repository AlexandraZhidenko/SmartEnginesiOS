    import XCTest
    @testable import SmartEngines

    final class SmartEnginesTests: XCTestCase {
        func testExample() {
            // This is an example of a functional test case.
            // Use XCTAssert and related functions to verify your tests produce the correct
            // results.
            XCTAssertEqual(SmartEngines().text, "Hello, World!")
        }
    }
